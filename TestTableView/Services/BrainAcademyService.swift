//
//  BrainAcademyService.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

protocol BrainAcademyServiceProtocol {
    func getLiveTeaching(page:Int, success:@escaping (_ response: LiveTeachingResponse)->(), failure: @escaping (WebService.ErrorAPI)->())
}

class BrainAcademyService: WebService, BrainAcademyServiceProtocol {
    func getLiveTeaching(page: Int, success: @escaping (_ response: LiveTeachingResponse) -> (), failure: @escaping (WebService.ErrorAPI) -> ()) {
        
        let rand = Int.random(in: 1...10)
        DispatchQueue.main.asyncAfter(deadline: .now()+3.0) {
            if rand > 8 {
                let errors: [ErrorAPI] = [
                    ErrorAPI(type: "", statusCode: 401, description: "Unautorizhed", errorCode: 401),
                    ErrorAPI(type: "", statusCode: 404, description: "Not Found", errorCode: 404),
                    ErrorAPI(type: "", statusCode: 500, description: "Server Error", errorCode: 500)
                ]
                failure(errors.randomElement()!)
            } else {
                let response = self.mockLiveTeachingResponse(page: page)
                success(response)
            }
        }
    }
}

extension BrainAcademyService {
    func mockLiveTeachingResponse(page: Int) -> LiveTeachingResponse {
        let data = LiveTeaching.mockData
        let page = page
        let rangeSize = 5
        let first = (page-1)*rangeSize
        let end = min(page*rangeSize, data.count)
        
        var items: [LiveTeaching]
        if page < 0 || first >= end {
            items = []
        } else {
            let arraySlice = data[first..<end]
            items = Array(arraySlice)
        }
        
        let liveData = LiveTeachingData(currentPage: page, totalPage: (data.count/rangeSize)+1, totalData: data.count, liveTeachings: items)
        
        return LiveTeachingResponse(status: "success", message: "Success get data", data: liveData)
    }
}
