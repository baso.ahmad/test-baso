//
//  WebService.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation
class WebService {
    struct ErrorAPI: Error {
        let type: String
        let statusCode:Int?
        let description: String
        let errorCode: Int
    }
}

