//
//  LoginService.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

protocol LoginServiceProtocol {
    func login(username: String, password: String, success:@escaping (_ response: LoginResponse)->(), failure: @escaping (WebService.ErrorAPI)->())
}

class LoginService: WebService, LoginServiceProtocol {
    func login(username: String, password: String, success: @escaping (LoginResponse) -> (), failure: @escaping (WebService.ErrorAPI) -> ()) {
        let rand = Int.random(in: 1...10)
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            if rand > 9 {
                let errors: [ErrorAPI] = [
                    ErrorAPI(type: "", statusCode: 400, description: "No Internet Connection", errorCode: 400)
                ]
                failure(errors.randomElement()!)
            } else {
                let response = self.mockLoginResponse(username: username, password: password)
                switch response {
                case .success(let response):
                    success(response)
                case .failure(let error):
                    failure(error)
                }
            }
        }
    }
}

extension LoginService {
    func mockLoginResponse(username: String, password: String) -> Result<LoginResponse, ErrorAPI> {
        let users = User.mockData
        if let user = users.filter({ user in
            user.username.lowercased() == username.lowercased() && user.password == password
        }).first {
            return .success(LoginResponse(status: "success", message: "Successfully Login", data: .init(user: user)))
        } else {
            return .failure(ErrorAPI(type: "", statusCode: 401, description: "Username / Password is Invalid", errorCode: 401))
        }
    }
}
