//
//  LiveTeaching.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

enum LiveTeachingStatus: String, Codable{
    case live = "Live"
    case pending = "Akan Datang"
}

struct LiveTeaching: Codable {
    let id: Int
    let status: LiveTeachingStatus
    let thumbnailUrl: String?
    let title: String
    let teachers: [String]
    let startDateTime: String
    let endDateTime: String
    
}

extension LiveTeaching {
    static let mockData: [LiveTeaching] =
        [
            LiveTeaching(id: 1, status: .live, thumbnailUrl: "https://mmc.tirto.id/image/otf/880x495/2020/01/03/header-hn-ruangguru_ratio-16x9.jpeg", title: "Trial Class Matematika Dimensi Tiga 12 IPA/IPS", teachers: ["Kak Tari"], startDateTime: "2021-07-09 12:00:00", endDateTime: "2021-07-09 13:00:00"),
            LiveTeaching(id: 2, status: .live, thumbnailUrl: "https://cdn-2.tstatic.net/tribunnews/foto/bank/images/ruangguru-konser-5-tahun.jpg", title: "Kelas Matematika 12 SMA", teachers: ["Kak Ade"], startDateTime: "2021-07-09 12:00:00", endDateTime: "2021-07-09 12:30:00"),
            LiveTeaching(id: 3, status: .live, thumbnailUrl: "https://cdn.sindonews.net/dyn/620/content/2020/01/16/144/1499053/keluarga-ruangguru-inspirasi-siswa-lebih-giat-belajar-1lU-thumb.jpg", title: "Trial Class IPA Terpadu Listrik Statis 9 SMP", teachers: ["Kak Nissa"], startDateTime: "2021-07-09 12:15:00", endDateTime: "2021-07-09 12:45:00"),
            LiveTeaching(id: 4, status: .live, thumbnailUrl: "https://cdns.klimg.com/merdeka.com/i/w/news/2019/10/31/1122097/670x335/ruangguru-ambisi-kami-bukan-menjadi-unicorn.jpg", title: "Kelas Sejarah 8 SMP", teachers: ["Kak Rizqi"], startDateTime: "2021-07-09 13:00:00", endDateTime: "2021-07-09 13:40:00"),
            LiveTeaching(id: 5, status: .live, thumbnailUrl: "https://1.bp.blogspot.com/-CXmmhWe92xc/XP9wOmjSJ8I/AAAAAAAAD5s/9tlRuAS3Je8oU0OISlYSuxioE5ypStTfgCLcBGAs/s640/ruangguru31.jpg", title: "Kelas Biologi 10 SMA", teachers: ["Kak Indah", "Kak Fadli"], startDateTime: "2021-07-09 13:15:00", endDateTime: "2021-07-09 14:00:00"),
            
            LiveTeaching(id: 6, status: .live, thumbnailUrl: "https://mmc.tirto.id/image/otf/880x495/2020/01/03/header-hn-ruangguru_ratio-16x9.jpeg", title: "Kelas Geografi 7 SMP", teachers: ["Kak Yusuf"], startDateTime: "2021-07-09 14:00:00", endDateTime: "2021-07-09 15:00:00"),
            LiveTeaching(id: 7, status: .live, thumbnailUrl: "https://cdn-2.tstatic.net/tribunnews/foto/bank/images/ruangguru-konser-5-tahun.jpg", title: "Kelas Bahasa Inggris 6 SD", teachers: ["Kak Ahmad"], startDateTime: "2021-07-09 15:00:00", endDateTime: "2021-07-09 16:00:00"),
            LiveTeaching(id: 8, status: .live, thumbnailUrl: "https://cdn.sindonews.net/dyn/620/content/2020/01/16/144/1499053/keluarga-ruangguru-inspirasi-siswa-lebih-giat-belajar-1lU-thumb.jpg", title: "Kelas Kimia 10 SMA", teachers: ["Kak Agus"], startDateTime: "2021-07-09 17:00:00", endDateTime: "2021-07-09 17:30:00"),
            LiveTeaching(id: 9, status: .live, thumbnailUrl: "https://cdns.klimg.com/merdeka.com/i/w/news/2019/10/31/1122097/670x335/ruangguru-ambisi-kami-bukan-menjadi-unicorn.jpg", title: "Kelas Fisika 9 SMP", teachers: ["Kak Ryan"], startDateTime: "2021-07-09 17:00:00", endDateTime: "2021-07-09 18:00:00"),
            LiveTeaching(id: 10, status: .pending, thumbnailUrl: "https://1.bp.blogspot.com/-CXmmhWe92xc/XP9wOmjSJ8I/AAAAAAAAD5s/9tlRuAS3Je8oU0OISlYSuxioE5ypStTfgCLcBGAs/s640/ruangguru31.jpg", title: "Kelas Biologi 8 SMP", teachers: ["Kak Adam"], startDateTime: "2021-07-09 19:00:00", endDateTime: "2021-07-09 19:45:00"),
        
            LiveTeaching(id: 11, status: .pending, thumbnailUrl: "https://mmc.tirto.id/image/otf/880x495/2020/01/03/header-hn-ruangguru_ratio-16x9.jpeg", title: "Kelas Bahasa Indonesia 9 SMP", teachers: ["Kak Dian"], startDateTime: "2021-07-09 19:30:00", endDateTime: "2021-07-09 19:40:00"),
            LiveTeaching(id: 12, status: .pending, thumbnailUrl: "https://cdn-2.tstatic.net/tribunnews/foto/bank/images/ruangguru-konser-5-tahun.jpg", title: "Kelas Matematika 10 SMA", teachers: ["Kak Adi"], startDateTime: "2021-07-09 20:00:00", endDateTime: "2021-07-09 20:30:00"),
            LiveTeaching(id: 13, status: .pending, thumbnailUrl: "https://cdn.sindonews.net/dyn/620/content/2020/01/16/144/1499053/keluarga-ruangguru-inspirasi-siswa-lebih-giat-belajar-1lU-thumb.jpg", title: "Kelas Ekonomi 12 SMA IPS", teachers: ["Kak Eko"], startDateTime: "2021-07-09 20:00:00", endDateTime: "2021-07-09 20:45:00"),
        ]
    
}
