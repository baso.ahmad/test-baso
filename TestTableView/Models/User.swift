//
//  User.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

struct User: Codable {
    let userId: Int
    let username, password, name: String
}

extension User {
    static let mockData: [User] =
        [
            User(userId: 1, username: "baso", password: "baso123", name: "Baso Ahmad Muflih"),
            User(userId: 2, username: "ahmad", password: "ahmad123", name: "Ahmad Muflih Baso"),
            User(userId: 3, username: "muflih", password: "muflih123", name: "Muflih Baso Ahmad"),
        ]
}
