//
//  LiveTeachingResponse.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

struct LiveTeachingResponse: Codable {
    var status, message: String
    var data: LiveTeachingData
}

struct LiveTeachingData: Codable {
    var currentPage, totalPage, totalData: Int
    var liveTeachings: [LiveTeaching]
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case totalPage = "total_page"
        case totalData = "total_data"
        case liveTeachings = "live_teachings"
    }
}
