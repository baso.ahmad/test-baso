//
//  LoginResponse.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

struct LoginResponse: Codable {
    var status, message: String
    var data: LoginData
}



struct LoginData: Codable {
    var user: User
}
