//
//  UIKitExtension.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import UIKit



extension UIView {
    func rounded(radius: CGFloat = 8){
            layer.cornerRadius = radius
        }
    func rounded(_ corners: UIRectCorner, radius: CGFloat = 8) {
        DispatchQueue.main.async {
        let path = UIBezierPath(roundedRect: self.bounds,
                             byRoundingCorners: corners,
                             cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
        }
    }
    
    /**
     loadingViewTag as an id
     */
    static let loadingViewTag = 1938123987
    
    /**
     Add and animate loading spinner to the center of current UIView
     */
    func showLoading(style: UIActivityIndicatorView.Style? = nil) {
        var finalStyle: UIActivityIndicatorView.Style!
        if style == nil {
            if #available(iOS 13, *) {
                finalStyle = .large
            } else {
                finalStyle = .gray
            }
        }
        var loading = viewWithTag(UIView.loadingViewTag) as? UIActivityIndicatorView
        if loading == nil {
            loading = UIActivityIndicatorView(style: finalStyle)
        }
        loading?.layer.zPosition = 1
        
        loading!.startAnimating()
        loading!.hidesWhenStopped = true
        loading?.tag = UIView.loadingViewTag
        addSubview(loading!)
        
        
        loading?.translatesAutoresizingMaskIntoConstraints = false
        loading?.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        loading?.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
    }
    
    /**
     Remove loading spinner
     */
    func stopLoading() {
        let loading = viewWithTag(UIView.loadingViewTag) as? UIActivityIndicatorView
        loading?.stopAnimating()
        loading?.removeFromSuperview()
    }
}

//MARK: - UITableView
extension UITableView{
    /**
     Set no data label with text of given `message`
     - Parameter message: message of the label
     */
    func setBackgroundText(_ message: String) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        label.text = message
        label.textAlignment = .center
        label.sizeToFit()

        self.backgroundView = label
    }
    
    func setBackgroundImage(_ image: UIImage, message: String? = "No Data") {
        
        
        let containerView = UIView()
        self.backgroundView = containerView
        
        let imageView = UIImageView()
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        label.text = message
        label.textAlignment = .center
        label.sizeToFit()
        
        containerView.addSubview(imageView)
        containerView.addSubview(label)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            imageView.widthAnchor.constraint(equalToConstant: 300),
            imageView.heightAnchor.constraint(equalToConstant: 300),
            imageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            label.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 32),
            label.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
        ]
        NSLayoutConstraint.activate(constraints)
        
        
    }
    
    /**
     Remove no data placeholder
     */
    func removeBackgroundView() {
        self.backgroundView = nil
    }
    
    /**
     Add loading spinner at the bottom of the current UITableView
     */
    func showFooterLoading(){
        var spinner: UIActivityIndicatorView!
        if #available(iOS 13.0, *) {
            spinner = UIActivityIndicatorView(style: .large)
        } else {
            spinner = UIActivityIndicatorView(style: .gray)
        }
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.bounds.width, height: CGFloat(44))

        self.tableFooterView = spinner
        self.tableFooterView?.isHidden = false
    }
    
    /**
     Remove loading spinner at the bottom of the current UITableView
     */
    func removeFooterLoading(){
        if let spinner = self.tableFooterView as? UIActivityIndicatorView {
            spinner.stopAnimating()
        }
        self.tableFooterView = nil
    }
    
    /**
     Add refresh control at the top of the current UITableView
     */
    func createRefreshControl(){
        self.refreshControl = UIRefreshControl()
    }

    /**
     Remove refresh control at the bottom of the current UITableView
     */
    func removeRefreshControl() {
        self.refreshControl?.endRefreshing()
        self.refreshControl = nil
        
    }
    
}

import NotificationBannerSwift

extension UIViewController {
    func showBanner(with info: BannerInfo){
        NotificationBannerQueue.default.dismissAllForced()
        let banner = FloatingNotificationBanner(
            title: info.title,
            subtitle: info.message,
            style: info.style
        )
        banner.show(cornerRadius: 8)
    }
}
