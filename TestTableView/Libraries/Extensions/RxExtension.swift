//
//  RxExtension.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//


import RxSwift
import RxCocoa



extension Reactive where Base: UITableView {
    /**
     Binder to show separatorStyle
     */
    public var separatorStyle: Binder<UITableViewCell.SeparatorStyle> {
           return Binder(base) { tableView, style in
                tableView.separatorStyle = style
           }
       }
    /**
     Binder to show/hide empty message on the current UITableView
     - Parameter message: Empty message want to be displayed
     */
    func isEmpty(message: String) -> Binder<Bool> {
        return Binder(base) { tableView, isEmpty in
            if isEmpty {
                tableView.setBackgroundText(message)
            } else {
                tableView.removeBackgroundView()
            }
        }
    }
    
    /**
     Binder to show/hide empty image on the current UITableView
     - Parameter image: Empty image want to be displayed
     */
    func isEmpty(image: UIImage, message: String? = nil) -> Binder<Bool> {
        return Binder(base) { tableView, isEmpty in
            if isEmpty {
                tableView.setBackgroundImage(image, message: message)
            } else {
                tableView.removeBackgroundView()
            }
        }
    }
    
    /**
     Binder to show/hide loading state of the current UITableView
     - Parameter hasRefreshControl: true if tableView has UIRefreshControl
     */
    func isLoading() -> Binder<Bool> {
        return Binder(base) { tableView, isLoading in
            if isLoading {
                if tableView.refreshControl != nil && tableView.refreshControl!.isRefreshing {
                    return
                }
                if tableView.numberOfRows(inSection: 0) == 0{
                    tableView.showLoading()
                } else {
                    tableView.showFooterLoading()
                }
                
                
            } else {
                tableView.removeFooterLoading()
                tableView.stopLoading()
                tableView.refreshControl?.endRefreshing()
            }
        }
    }
}

extension Reactive where Base: UIView {
    /**
     Binder to show/hide loading state of the current UIView
     */
    func showLoading() -> Binder<Bool> {
        return Binder(base) { uiView, isLoading in
            if isLoading {
                uiView.showLoading()
            } else {
                uiView.stopLoading()
            }
        }
    }
}

extension Reactive where Base: UITabBarItem {
    /**
     Binder to show badge number on the current UITabBarItem
     */
    public var badgeNumber: Binder<Int> {
        return Binder(base) { tabBar, number in
            if number > 0 {
                tabBar.badgeValue = String(number)
            } else {
                tabBar.badgeValue = nil
            }
        }
    }
}



//extension Reactive where Base: SkyFloatingLabelTextField {
//    /**
//     Binder to show/hide errorMessage in the current SkyFloatingLabelTextField
//     */
//    public var errorMessage: Binder<String?> {
//        return Binder(base) { textField, errorMessage in
//            textField.errorMessage = errorMessage
//        }
//    }
//}
