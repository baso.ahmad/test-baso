//
//  TableViewCell.swift
//  TestTableView
//
//  Created by Ruangguru on 08/07/21.
//

import UIKit

protocol TableViewModel {
    
}

protocol TableViewCellProtocol {
    associatedtype T: TableViewModel
    static var identifier: String { get }
    var viewModel: T! { get set }
    func setData()
    static func nib() -> UINib
}

extension TableViewCellProtocol {
    static func nib() -> UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
}

typealias TableViewCell = TableViewCellProtocol & UITableViewCell
