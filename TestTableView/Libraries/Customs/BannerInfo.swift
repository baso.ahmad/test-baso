//
//  BannerInfo.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import NotificationBannerSwift

struct BannerInfo {
    var title: String?
    var message: String
    var style: BannerStyle
}
