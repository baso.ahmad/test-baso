//
//  LoadMoreTableView.swift
//  students
//
//  Created by Solihin Chiko on 05/11/18.
//  Copyright © 2018 Ruangguru. All rights reserved.
//

import UIKit

class LoadMoreTableView: UITableView {
    enum LoadMoreState {
        case idle
        case loading
        case done
    }
    var loadMore: (()->Void)?
    var pullToRefresh: (()->Void)?
    var loadMoreState: LoadMoreState = .loading {
        didSet {
            if loadMoreState == .idle {
                addBottomScrollObservation()
            } else {
                removeBottomScrollObservation()
            }
        }
    }
    private var contentObservation: NSKeyValueObservation?
    
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        removeBottomScrollObservation()
    }

    // MARK: - KVO
    fileprivate func addBottomScrollObservation() {
        self.contentObservation = self.observe(\.contentOffset) { [weak self] view, _ in
            
            self?.handleContentOffsetChange()
            
        }
    }
    
    fileprivate func removeBottomScrollObservation() {

        guard contentObservation != nil else { return }
        contentObservation?.invalidate()
        contentObservation = nil
    }
    
    fileprivate func handleContentOffsetChange() {
        guard self.loadMoreState == .idle, self.numberOfRows(inSection: 0) > 0 else { return }
//        DispatchQueue.main.async {
            guard self.contentOffset.y > self.contentSize.height - (self.bounds.height + self.contentInset.bottom) else { return }
            self.loadMore?()
//        }
    }
    
    override func reloadData() {
        super.reloadData()
    }
    
    func isCanRefresh(_ canRefresh: Bool) {
        guard canRefresh else { return }
        refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(self.refreshControlDidPulled(_:)), for: .valueChanged)
    }
    
    
    
    // MARK: - Call back delegate
    @objc func refreshControlDidPulled(_ control:UIRefreshControl) {
        pullToRefresh?()
    }
    
    
    func scrollToTop(){
        let indexPath = IndexPath(row: 0, section: 0)
        self.scrollToRow(at: indexPath, at: .top, animated: false)
    }
}

