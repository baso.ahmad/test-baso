//
//  DateUtils.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

enum DateStringFormat: String {
    case dateTime = "yyyy-MM-dd HH:mm:ss"
    case date = "yyyy-MM-dd"
    case formattedShortDate = "d MMM yyyy"
    case formattedDate = "d MMMM yyyy"
    case formattedDate2 = "d/M/yyyy"
    case formattedDateTime = "d/M/yyyy HH:mm"
    case formattedDateMonth = "d MMM"
    case formattedYearMonth = "yyyy-MM"
    case formattedDayDateTime = "EEEE, d MMMM yyyy HH:mm"
    case formattedDayDate = "EEEE, d MMMM yyyy"
    case formattedDateDateTime = "d MMMM yyyy, HH:mm"
    case formattedTime = "HH:mm:ss"
    case formattedHourMinute = "HH:mm"
}

class DateUtils {
    /**
     Transform a string of date into a Date object

     - Parameter dateString: the String of date.
     - Parameter fromFormat: the format of dateString. For example : format of "2021-05-24" is "yyyy-MM-dd".
     - Returns: Return the date object of given date string, if input is not valid return nil
     */
    
    static func stringToDate(dateString: String?, fromFormat: DateStringFormat) -> Date? {
        guard let dateString = dateString else { return nil }
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.locale = Locale(identifier: "id_ID")
        inputDateFormatter.dateFormat = fromFormat.rawValue
        return inputDateFormatter.date(from: dateString)
    }
    
    /**
     Transform a string of date into a Date object

     - Parameter dateString: the String of date.
     - Parameter fromFormat: the format of dateString. For example : format of "2021-05-24" is "yyyy-MM-dd".
     - Parameter toFormat: the format of dateString you want.
     - Returns: Return  a date string of given date string another forrmat, if input is not valid return nil
     */
    
    static func transformDate(dateString: String?, fromFormat: DateStringFormat, toFormat: DateStringFormat) -> String? {
        guard let dateString = dateString else { return nil }
        let date = stringToDate(dateString: dateString, fromFormat: fromFormat)
        return transformDate(date: date, toFormat: toFormat)
    }
    
    /**
     Transform a Date object into a a string of date

     - Parameter date: the object of a date
     - Parameter toFormat: the format of dateString you want.
     - Returns: Return a date string of given date object, if input is not valid return nil
     */
    static func transformDate(date: Date?, toFormat: DateStringFormat) -> String? {
        if let date = date {
            let outputDateFormatter = DateFormatter()
            outputDateFormatter.dateFormat = toFormat.rawValue
            outputDateFormatter.locale = Locale(identifier: "id_ID")
            return outputDateFormatter.string(from: date)
        }
        return nil
    }
    
    /**
     Transform a number into currency format of Rupiah / IDR

     - Parameter number: number you want to convert
     - Returns: Text of the `number` in currency format. For example: Rp 1.000,00
     */
    
    static func transformRp(_ number: Int) -> String?{
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: number as NSNumber) {
           return "Rp " + formattedTipAmount + ",00"
        }
        return nil
    }
    
    /**
     Transform a number (minutes) into string format of HH:mm

     - Parameter minutes: number of minutes
     - Returns: Return string format of HH:mm
     */
    static func minutesToHHmm(minutes: Int) -> String {
        let hours: Int = minutes / 60
        let minutes: Int = minutes % 60
        return String(format: "%02d:%02d", hours, minutes)
    }
    
    /**
     Transform string format of HH:mm into number of minutes

     - Parameter time: string with format of HH:mm
     - Returns: Return number of minutes. If input is not valid, return nil
     */
    static func hhmmToMinutes(time: String?) -> Int? {
        if let time = time {
            let timeArr: Array = time.components(separatedBy: ":")
            guard let hours = Int(timeArr[0]) else { return nil }
            guard let minutes = Int(timeArr[1]) else { return nil }
            return Int((hours * 60) + (minutes))
        }
        return nil
    }
}
