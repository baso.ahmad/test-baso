//
//  LiveViewModel.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import RxSwift
import RxCocoa

class LiveViewModel {
    let title: String
    let liveID: Int
    
    let isLoading = BehaviorRelay<Bool>(value: false)
    init(with liveTeaching: LiveTeaching){
        self.title = liveTeaching.title
        self.liveID = liveTeaching.id
        
        isLoading.accept(true)
    }
}
