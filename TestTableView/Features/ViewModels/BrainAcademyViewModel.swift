//
//  BrainAcademyViewModel.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import RxSwift
import RxCocoa


class BrainAcademyViewModel {
    let items = BehaviorRelay<[LiveTeaching]>(value: [])
    let isLoading: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    let errorMessage = PublishSubject<String>()
    var currentPage = 0
    var totalPage = 1
    var page: Int!
    var hasMoreData: Bool {
        return totalPage > currentPage
    }
    
    let service: BrainAcademyServiceProtocol
    
    init(service: BrainAcademyServiceProtocol = BrainAcademyService()){
        self.service = service
    }
    
    func fetchData(reload: Bool = true) {
        if (!isLoading.value && (reload || !reload && hasMoreData)){
            isLoading.accept(true)
            page = currentPage
            if reload {
                page = 1
            } else {
                page += 1
            }
            fetchApi()
        }
    }
    
    func getItems() -> Observable<[LiveItemTableViewModel]> {
        return self.items.map { items in
            items.map {
                LiveItemTableViewModel(with: $0)
            }
        }
    }
    
    private func fetchApi(){
        service.getLiveTeaching(page: page, success: { [weak self] response in
            self?.totalPage = response.data.totalPage
            self?.onSuccess(items: response.data.liveTeachings)
            
        }, failure: { [weak self] error in
            self?.onError(error)
        })
    }
    
    private func onSuccess(items: [LiveTeaching]){
        if page == 1 {
            self.items.accept(items)
        } else {
            self.items.accept(self.items.value+items)
        }
        self.currentPage = page
        self.isLoading.accept(false)
    }
    
    private func onError(_ error: WebService.ErrorAPI){
        self.errorMessage.onNext(error.description)
        self.isLoading.accept(false)
    }
    
    func showEmpty() -> Observable<Bool> {
        return Observable.combineLatest(items, isLoading){
            $0.count == 0 && !$1
        }.distinctUntilChanged()
    }
}
