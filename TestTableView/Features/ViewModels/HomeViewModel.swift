//
//  HomeViewModel.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import RxSwift
import RxCocoa

class HomeViewModel {
    var name: Observable<String?> {
        UserDefaults.standard.rx.observe(String.self, SessionKey.name)
    }
    
    
    var hasLogin: Observable<Bool> {
        UserDefaults.standard.rx.observe(Int.self, SessionKey.userId).map { $0 != nil }
    }
    
    init(){
    }
    
    func logout() {
        UserDefaults.standard.removeObject(forKey: SessionKey.name)
        UserDefaults.standard.removeObject(forKey: SessionKey.userId)
    }
}
