//
//  LoginViewModel.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import RxSwift
import RxCocoa

class LoginViewModel {
    let usernameText = BehaviorRelay<String?>(value: nil)
    let passwordText = BehaviorRelay<String?>(value: nil)
    let info = PublishSubject<BannerInfo>()
    let isLoading = PublishSubject<Bool>()
    let isFinished = PublishSubject<Bool>()
    let service: LoginServiceProtocol
    init(service: LoginServiceProtocol = LoginService()){
        self.service = service
    }
    
    func login(){
        if let username = usernameText.value, let password = passwordText.value {
            self.isLoading.onNext(true)
            service.login(username: username, password: password, success: { [weak self] response in
                self?.saveUser(response.data.user)
                self?.info.onNext(.init(title: nil, message: response.message, style: .success))
                self?.isLoading.onNext(false)
                self?.isFinished.onNext(true)
            }, failure: { [weak self] error in
                self?.isLoading.onNext(false)
                self?.info.onNext(.init(title: nil, message: error.description, style: .danger))
            })
        }
    }
    
    func saveUser(_ user: User){
        UserDefaults.standard.setValue(user.userId, forKey: SessionKey.userId)
        UserDefaults.standard.setValue(user.name, forKey: SessionKey.name)
    }
    
    func isFormValid() -> Observable<Bool> {
        Observable.combineLatest(usernameText, passwordText){ username, password in
            return username != nil && username?.trimmingCharacters(in: .whitespacesAndNewlines) != "" && password != nil && password != ""
        }.distinctUntilChanged()
    }
}
