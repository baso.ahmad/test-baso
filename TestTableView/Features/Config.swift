//
//  Config.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

struct SessionKey {
    static let userId = "user_id"
    static let name = "name"
}
