//
//  LiveItemTableViewModel.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import UIKit

struct LiveItemTableViewModel: TableViewModel {
    let data: LiveTeaching
    var statusColor: UIColor
    var status: String
    var canEnter: Bool
    var thumbnailURL: URL?
    var title, teacher, date, time: String?
    
    init(with data: LiveTeaching){
        self.data = data
        switch data.status {
        case .live:
            self.statusColor = .red
            self.canEnter = true
        case .pending:
            self.statusColor = .blue
            self.canEnter = false
        }
        self.status = data.status.rawValue
        self.thumbnailURL = URL(string: data.thumbnailUrl ?? "")
        self.title = data.title
        self.teacher = data.teachers.joined(separator: ", ")
        self.date = DateUtils.transformDate(dateString: data.startDateTime, fromFormat: .dateTime, toFormat: .formattedDayDate)
        
        if let startTime = DateUtils.transformDate(dateString: data.startDateTime, fromFormat: .dateTime, toFormat: .formattedHourMinute), let endTime = DateUtils.transformDate(dateString: data.endDateTime, fromFormat: .dateTime, toFormat: .formattedHourMinute) {
            self.time = "\(startTime) - \(endTime)"
        }
    }
}
