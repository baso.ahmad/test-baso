//
//  LiveItemTableViewCell.swift
//  TestTableView
//
//  Created by Ruangguru on 08/07/21.
//

import UIKit
import Kingfisher

class LiveItemTableViewCell: TableViewCell {
    
    static var identifier: String = "LiveItemTableViewCell"
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var thumbnailView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var teacherLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var startClassButton: UIButton!
    
    var viewModel: LiveItemTableViewModel!{
        didSet{
            setData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.rounded(radius: 16)
        thumbnailView.rounded([.topLeft, .topRight], radius: 16)
        statusView.layer.cornerRadius = 4
        startClassButton.layer.cornerRadius = 12
        startClassButton.isUserInteractionEnabled = false
    }
    func setData() {
        statusView.backgroundColor = viewModel.statusColor
        statusLabel.text = viewModel.status
        if let imageUrl = viewModel.thumbnailURL {
            let processor = DownsamplingImageProcessor(size: thumbnailView.bounds.size)
            thumbnailView.kf.setImage(
                with: imageUrl,
                placeholder: nil,
                options: [
                    .processor(processor),
                    .loadDiskFileSynchronously,
                    .cacheOriginalImage
                ]
            )
        }
        titleLabel.text = viewModel.title
        teacherLabel.text = viewModel.teacher
        dateLabel.text = viewModel.date
        timeLabel.text = viewModel.time
        startClassButton.isHidden = !viewModel.canEnter
    }
}
