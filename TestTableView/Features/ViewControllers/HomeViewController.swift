//
//  ViewController.swift
//  TestTableView
//
//  Created by Ruangguru on 08/07/21.
//

import UIKit
import RxSwift

class HomeViewController: UIViewController, Storyboarded {
    weak var coordinator: HomeCoordinator?
    let viewModel = HomeViewModel()
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    let disposeBag = DisposeBag()
    
    var hasLogin = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        dataBinding()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
            print(self.coordinator?.childCoordinators.count)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func dataBinding(){
        viewModel
            .hasLogin
            .bind { [weak self] hasLogin in
                guard let self = self else { return }
                self.loginButton.isHidden = hasLogin
                self.logoutButton.isHidden = !hasLogin
                self.hasLogin = hasLogin
            }.disposed(by: disposeBag)
        
        viewModel
            .name
            .map { name -> String in
                if name == nil {
                    return "Hi, Welcome to Ruangguru"
                } else {
                    return "Hi, \(name!)"
                }
            }.bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    @IBAction func didTapBrainAcademyButton(_ sender: UIButton) {
        if hasLogin {
            coordinator?.goTo(target: .brainAcademy)
        } else {
            self.showBanner(with: .init(title: nil, message: "Please login first!", style: .warning))

        }
    }
    
    @IBAction func didTapLoginButton(_ sender: UIButton) {
        coordinator?.goTo(target: .login)
    }
    
    @IBAction func didTapLogoutButton(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: "Are you sure want to logout?", preferredStyle: .actionSheet)
        alertController.addAction(.init(title: "Logout", style: .destructive) { [weak self] _ in
            self?.viewModel.logout()
        })
        alertController.addAction(.init(title: "Cancel", style: .default))
        self.present(alertController, animated: true)
    }
    
}

