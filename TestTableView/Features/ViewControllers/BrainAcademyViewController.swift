//
//  BrainAcademyViewController.swift
//  TestTableView
//
//  Created by Ruangguru on 08/07/21.
//

import UIKit
import RxSwift
import NotificationBannerSwift

protocol RxControllerProtocol {
    func dataBinding()
}

class BrainAcademyViewController: UIViewController, RxControllerProtocol {
    weak var coordinator: BrainAcademyCoordinator?
    let viewModel = BrainAcademyViewModel()
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var tableView: LoadMoreTableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Brain Academy"
        
        reloadData()
        registerTable()
        
        dataBinding()
        
    }
    
    func registerTable() {
        
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        
        tableView.register(LiveItemTableViewCell.nib(), forCellReuseIdentifier: LiveItemTableViewCell.identifier)
        
        viewModel
            .getItems()
//            .observe(on: MainScheduler.asyncInstance)
            .bind(to: tableView.rx.items(cellIdentifier: LiveItemTableViewCell.identifier, cellType: LiveItemTableViewCell.self)) { row, viewModel, cell in
                cell.viewModel =  viewModel
                cell.backgroundColor = .clear
                cell.selectionStyle = .none
        }.disposed(by: disposeBag)
        
        tableView
            .rx
            .modelSelected(LiveItemTableViewModel.self)
            .bind{ [weak self] viewModel in
                self?.onItemClick(model: viewModel)
            }.disposed(by: disposeBag)
        
        tableView.isCanRefresh(true)
        
        tableView.pullToRefresh = { [weak self] in
            self?.reloadData()
        }
        
        self.tableView.loadMore = { [weak self] in
            guard let self = self else { return }
            if self.viewModel.hasMoreData {
                self.viewModel.fetchData(reload: false)
            } else {
                self.tableView.loadMoreState = .done
            }
        }
    }
    
    func dataBinding() {
        
        viewModel
            .isLoading.distinctUntilChanged()
            .bind{ [weak self] isLoading in
                self?.tableView.rx.isLoading().onNext(isLoading)
                self?.tableView.loadMoreState = isLoading ? .loading : .idle
            }
            .disposed(by: disposeBag)
        
        viewModel
            .showEmpty()
            .distinctUntilChanged()
            .bind(to: tableView.rx.isEmpty(image: UIImage(named: "imgEmpty")!, message: "No Live Teaching!"))
            .disposed(by: disposeBag)
        
        viewModel
            .errorMessage
            .bind { [weak self] error in
                self?.showBanner(with: .init(title: nil, message: error, style: .danger))
        }.disposed(by: disposeBag)
    }

    func onItemClick(model: LiveItemTableViewModel){
        coordinator?.goTo(target: .live(data: model.data))
    }
    
    func reloadData(){
        viewModel.fetchData(reload: true)
    }
    
    func showError(message: String) {
        NotificationBannerQueue.default.dismissAllForced()
        let banner = FloatingNotificationBanner(
            title: nil,
            subtitle: message,
            style: .danger
        )
        banner.show(cornerRadius: 8)
    }
}
