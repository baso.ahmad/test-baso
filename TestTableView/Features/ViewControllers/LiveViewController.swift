//
//  LiveViewController.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import UIKit
import RxSwift

class LiveViewController: UIViewController {
    weak var coordinator: LiveCoordinator?
    let viewModel: LiveViewModel
    let disposeBag = DisposeBag()
    
    init(viewModel: LiveViewModel){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.title
        dataBinding()
    }
    
    func dataBinding(){
        viewModel
            .isLoading
            .bind(to: view.rx.showLoading())
            .disposed(by: disposeBag)
    }
}
