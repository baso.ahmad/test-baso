//
//  LoginViewController.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import UIKit
import RxSwift

class LoginViewController: UIViewController {
    weak var coordinator: LoginCoordinator?
    let viewModel = LoginViewModel()
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginContainer: UIView!
    @IBOutlet weak var loginButton: UIButton!
    
    let disposeBag = DisposeBag()
    
    deinit {
        coordinator?.parentCoordinator?.childDidFinish(coordinator)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupViews()
        dataBinding()
    }
    
    func setupViews(){
        stackView.setCustomSpacing(32, after: titleLabel)
        stackView.setCustomSpacing(24, after: passwordTextField)
    }
    
    func dataBinding(){
        usernameTextField.rx.text.bind(to: viewModel.usernameText).disposed(by: disposeBag)
        passwordTextField.rx.text.bind(to: viewModel.passwordText).disposed(by: disposeBag)
        
        viewModel.isFormValid().bind{ [weak self] isValid in
            self?.loginButton.backgroundColor = isValid ? .systemBlue : .systemGray
            self?.loginButton.isEnabled = isValid
        }.disposed(by: disposeBag)
        
        viewModel
            .isLoading
            .bind { [weak self] isLoading in
                self?.loginContainer.rx.showLoading().onNext(isLoading)
                self?.loginButton.isHidden = isLoading
            }
            .disposed(by: disposeBag)
        
        viewModel
            .isFinished
            .filter{ $0 }
            .bind { [weak self] _ in
                self?.dismiss()
            }.disposed(by: disposeBag)
        
        viewModel
            .info
            .bind { [weak self] info in
                self?.showBanner(with: info)
            }.disposed(by: disposeBag)
    }

    @IBAction func didTapLoginButton(_ sender: Any) {
        viewModel.login()
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        dismiss()
    }
    
    func dismiss(){
        self.dismiss(animated: true)
    }
}
