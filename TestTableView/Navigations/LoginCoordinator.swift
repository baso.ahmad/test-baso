//
//  LoginCoordinator.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import UIKit

class LoginCoordinator: Coordinator {
    
    weak var parentCoordinator: Coordinator?
    var childCoordinators = [Coordinator]()
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController){
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = LoginViewController()
        vc.coordinator = self
        vc.modalPresentationStyle = .formSheet
        navigationController.present(vc, animated: true)
    }
    
}
