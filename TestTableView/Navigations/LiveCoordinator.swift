//
//  LiveCoordinator.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import Foundation

import UIKit

class LiveCoordinator: Coordinator {
    
    weak var parentCoordinator: Coordinator?
    var childCoordinators = [Coordinator]()
    let liveTeaching: LiveTeaching
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController, data liveTeaching: LiveTeaching){
        self.navigationController = navigationController
        self.liveTeaching = liveTeaching
    }
    
    func start() {
        let vc = LiveViewController(viewModel: .init(with: liveTeaching))
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
}
