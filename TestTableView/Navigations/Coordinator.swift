//
//  Coordinator.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import UIKit

protocol CoordinatorTarget {
    associatedtype Target
    
    func goTo(target: Target)
}

protocol Coordinator: AnyObject {
    var parentCoordinator: Coordinator? { get set }
    var childCoordinators: [Coordinator]  { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
    func childDidFinish(_ child: Coordinator?)
}

extension Coordinator {
    func childDidFinish(_ child: Coordinator?){
        if let index = childCoordinators.firstIndex(where: {$0 === child}){
            childCoordinators.remove(at: index)
        }
    }
}
typealias RGCoordinator = Coordinator & CoordinatorTarget
