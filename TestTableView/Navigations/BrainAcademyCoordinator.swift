//
//  BrainAcademyCoordinator.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import UIKit

class BrainAcademyCoordinator: RGCoordinator {
    
    enum Target {
        case live(data: LiveTeaching)
    }
    
    weak var parentCoordinator: Coordinator?
    var childCoordinators = [Coordinator]()
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController){
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = BrainAcademyViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func goTo(target: Target) {
        switch target {
        case .live(let data):
            let child = LiveCoordinator(navigationController: navigationController, data: data)
            childCoordinators.append(child)
            child.start()
        }
    }
    
}
