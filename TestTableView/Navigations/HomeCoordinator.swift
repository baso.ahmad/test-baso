//
//  HomeCoordinator.swift
//  TestTableView
//
//  Created by Ruangguru on 09/07/21.
//

import UIKit

class HomeCoordinator: NSObject, RGCoordinator {
    weak var parentCoordinator: Coordinator?
    
    enum Target {
        case brainAcademy
        case login
    }
    var childCoordinators = [Coordinator]()
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController){
        self.navigationController = navigationController
    }
    
    func start() {
        navigationController.delegate = self
        let vc = HomeViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func goTo(target: Target){
        var child: Coordinator?
        switch target {
        case .brainAcademy:
            child = BrainAcademyCoordinator(navigationController: navigationController)
        case .login:
            child = LoginCoordinator(navigationController: navigationController)
            break
        }
        if let child = child {
            child.parentCoordinator = self
            childCoordinators.append(child)
            child.start()
        }
    }
    
}

extension HomeCoordinator: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }
        
        if navigationController.viewControllers.contains(fromViewController) {
            return
        }
        
        if let englishAcademyVC = fromViewController as? BrainAcademyViewController {
            childDidFinish(englishAcademyVC.coordinator)
        }
    }
}
