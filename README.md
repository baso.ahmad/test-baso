# TestBaso
A playground only to learn.. This consists :

# Features & Design Pattern
- MVVM for ViewControllers and UIView (including TableViewCell)
- RxSwift / RxCocoa as binding
- Showing list inside UITableView, Handling pagination, loading state and empty state
- Implementing Coordinator Pattern

# Requirements
- Min. iOS 11
- Swift 5+

# Libraries
- RxSwift & RxCocoa for Reactive Programming
- Kingfisher for loading images
- NotificationBannerSwift for displaying messages
- BMPlayer (soon) for playing videos
